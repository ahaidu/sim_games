# sim_games #

Interactive kitchen scenario games in gazebo simulator. 


### Prerequisites ###

 * [Gazebo](http://gazebosim.org/tutorials?tut=hydra&cat=user_input#RazerHydraconfiguration) with Razer Hydra config. 
 * [libconfig](http://www.hyperrealm.com/libconfig/)

~~~
sudo apt-get install libconfig++8-dev
~~~
 

### Build ###
~~~
mkdir build
cd build
cmake ..
make
~~~

### Set up plugin path ###
~~~
echo "export GAZEBO_PLUGIN_PATH=/<path>/sim_games/build:${GAZEBO_PLUGIN_PATH}" >> ~/.bashrc
source ~/.bashrc
~~~

### Usage ###
~~~
gazebo worlds/kitchen.world -u --verbose
~~~

### User Interaction ###

 * Razer Hydra device:
 * middle button -> starts/stops hand tracking
 * bumper button -> creates pancake
 * button 3 -> starts/stops logging the data