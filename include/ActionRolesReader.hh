/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2016, Andrei Haidu, Institute for Artificial Intelligence,
 *  Universität Bremen.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Institute for Artificial Intelligence,
 *     Universität Bremen, nor the names of its contributors may be
 *     used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/


#ifndef ACTION_ROLES_READER_PLUGIN_HH
#define ACTION_ROLES_READER_PLUGIN_HH

#include <gazebo/gazebo.hh>

namespace sim_games
{
    
/// \brief class ActionRolesReader
class ActionRolesReader : public gazebo::SystemPlugin
{
    /// \brief Constructor
    public: ActionRolesReader();

    /// \brief Destructor
    public: virtual ~ActionRolesReader();

    /// \brief Load plugin (Load called first, then Init)
    protected: virtual void Load(int /*_argc*/, char ** /*_argv*/);

    /// \brief Init plugin (Load called first, then Init)
    protected: virtual void Init();
    
    /// \brief Call after the world connected event
    private: void InitOnWorldConnect();
    
    /// \brief World created connection
    private: gazebo::event::ConnectionPtr worldCreatedConnection;

    /// \brief Gazebo communication node
    private: gazebo::transport::NodePtr gznode;

    /// \brief Custom event msg publisher
    private: gazebo::transport::PublisherPtr customEventPub;
    
    /// \brief PRAC events pub
    private: gazebo::transport::PublisherPtr pracEventPub;
    
    /// \brief Arguments from terminal
    private: std::string instruction;
    
    /// \brief Arguments from terminal
    private: std::string actionCore;    
    
    /// \brief Arguments from terminal
    private: std::string goal;
    
    /// \brief Arguments from terminal
    private: std::string amount;
    
    /// \brief Arguments from terminal
    private: std::string unit;
    
    /// \brief Arguments from terminal
    private: std::string stuff;  
    
    /// \brief Arguments from terminal
    private: std::string actionVerb;
    
    /// \brief Arguments from terminal
    private: std::string content;    
};
}


#endif
