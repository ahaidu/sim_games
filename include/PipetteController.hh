/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2016, Andrei Haidu,
 *  Institute for Artificial Intelligence, Universität Bremen.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Institute for Artificial Intelligence,
 *     Universität Bremen, nor the names of its contributors may be
 *     used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef PIPETTE_CONTROLLER_HH
#define PIPETTE_CONTROLLER_HH

#include "gazebo/physics/physics.hh"
#include "gazebo/common/Plugin.hh"

namespace sim_games
{   

/// \brief PipetteController class
class PipetteController : public gazebo::ModelPlugin
{
    /// \brief Constructor
    public: PipetteController();

    /// \brief Destructor
    public: virtual ~PipetteController();

    /// \brief Load plugin
    protected: virtual void Load(gazebo::physics::ModelPtr _parent, sdf::ElementPtr _sdf);

    /// \brief Callback button contact sensor
    private: void OnButtonContact(ConstContactsPtr &_msg);

    /// \brief Callback tip contact sensor
    private: void OnTipContact(ConstContactsPtr &_msg);

    /// \brief attach joint between the tip and the link in contact
    private: void AttachJoint(const std::string _model_name, const std::string _link_name);

    /// \brief detach joint from the end effector
    private: void DetachJoint();

    /// \brief World pointer
    private: gazebo::physics::WorldPtr world;

    /// \brief model of the robot hand
    private: gazebo::physics::ModelPtr pipetteModel;

    /// \brief world update event
    private: gazebo::event::ConnectionPtr updateConnection;

    /// \brief Node used for using Gazebo communications.
    private: gazebo::transport::NodePtr gznode;

    /// \brief Top button contact sub
    private: gazebo::transport::SubscriberPtr buttonContactSub;

    /// \brief Pippette tip contact sub
    private: gazebo::transport::SubscriberPtr tipContactSub;

    /// \brief Custom event msg publisher (e.g. start/stop logging)
    private: gazebo::transport::PublisherPtr customEventPub;

    /// \brief Joint pointer used to fixate on objects
    private: gazebo::physics::JointPtr fixedJoint;

    /// \brief Set of model names which can be pipetted
    private: std::set<std::string> liquidNames;
    
    /// \brief Name of the model held by the pipette
    private: std::string holdModelName;
    
    /// \brief Name of the link held by the pipette
    private: std::string holdLinkName;
    
    /// \brief Timestamp of pipetting start
    private: double pipettingStartTs;

    /// \brief Fixate joint attached to the end effector
    private: bool jointAttached;

    /// \brief Button is in contact with something
    private: bool buttonInContact;
    
    /// \brief Button pressed
    private: bool buttonPressedForDetach;
};
}
#endif
