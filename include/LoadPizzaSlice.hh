/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2015, Andrei Haidu,
 *  Institute for Artificial Intelligence, Universität Bremen.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Institute for Artificial Intelligence,
 *     Universität Bremen, nor the names of its contributors may be
 *     used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef LOAD_PIZZA_SLICE_HH
#define LOAD_PIZZA_SLICE_HH

#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>

namespace sim_games
{

/// \brief class LoadPizzaSlice
class LoadPizzaSlice : public gazebo::WorldPlugin
{
    /// \brief Constructor
    public: LoadPizzaSlice();

    /// \brief Destructor
    public: virtual ~LoadPizzaSlice();

    /// \brief Load plugin
    protected: virtual void Load(gazebo::physics::WorldPtr _parent, sdf::ElementPtr _sdf);

    /// \brief Generate sdf
    private: sdf::SDF GeneratePizzaSliceSDF(
            const gazebo::math::Vector3 _pos,
            const double _length_x,
            const double _length_y,
            const double _nr_x,
            const double _nr_y,
            const std::string _geometry,
            const gazebo::math::Vector3 _inertia_v,
            const double _unit_mass,
            const double _unit_size,
            const double _cfm,
            const double _erp
            );

    /// \brief Generate link sdf string
    private: std::string GenerateLink(
            const gazebo::math::Vector3 _link_pos,
            const std::string _geometry,
            const gazebo::math::Vector3 _inertia_v,
            const double _unit_mass,
            const double _unit_size,
            const unsigned int _it
            );

    /// \brief Generate joint string sdf
    private: std::string GenerateJoint(
            const gazebo::math::Vector3 _joint_pos,
            const unsigned int _parent_it,
            const unsigned int _child_it,
            const double _cfm,
            const double _erp);

};

}

#endif
