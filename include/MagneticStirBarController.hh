/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2016, Andrei Haidu,
 *  Institute for Artificial Intelligence, Universität Bremen.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Institute for Artificial Intelligence,
 *     Universität Bremen, nor the names of its contributors may be
 *     used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef MAGNETIC_STIR_CONTROLLER_HH
#define MAGNETIC_STIR_CONTROLLER_HH

#include "gazebo/physics/physics.hh"
#include "gazebo/common/Plugin.hh"
#include "gazebo/util/LogRecord.hh"

namespace sim_games
{   

/// \brief MagneticStirBarController class
class MagneticStirBarController : public gazebo::ModelPlugin
{
    /// \brief Constructor
    public: MagneticStirBarController();

    /// \brief Destructor
    public: virtual ~MagneticStirBarController();

    /// \brief Load plugin
    protected: virtual void Load(gazebo::physics::ModelPtr _parent, sdf::ElementPtr _sdf);
  
    /// \brief Update event callback
    private: void OnUpdate();
    
    /// \brief On button pressed contact sensor callback
    private: void OnButton(ConstContactsPtr &_msg);
    
    /// \brief apply the control forces
    private: void MagneticControl(const gazebo::common::Time _step_time);
    
    /// \brief Pointer to the update event connection
    private: gazebo::event::ConnectionPtr updateConnection;
    
    /// \brief World pointer
    private: gazebo::physics::WorldPtr world;
    
    /// \brief Model
    private: gazebo::physics::ModelPtr model;
    
    /// \brief Pos x PID 
    private: gazebo::common::PID xPosPID;
    
    /// \brief Pos y PID 
    private: gazebo::common::PID yPosPID;
    
    /// \brief Pos z PID 
    private: gazebo::common::PID zPosPID;
        
    /// \brief PID Values
    private: gazebo::math::Vector3 pidValues;
    
    /// \brief Prev timestamp
    private: gazebo::common::Time prevTimestamp;
    
    /// \brief Desired position of the model
    private: gazebo::math::Vector3 desiredPosition;
    
    /// \brief Desired orientation of the model
    private: gazebo::math::Quaternion desiredQuaternion;
    
    /// \brief Magnetic stirrer model
    private: gazebo::physics::ModelPtr magneticStirrer;
    
    /// \brief Node used for using Gazebo communications.
    private: gazebo::transport::NodePtr gznode;

    /// \brief Subscribe to button contact topic.
    private: gazebo::transport::SubscriberPtr buttonSub;
    
    /// \brief Publish custom events (e.g. button pressed)
    private: gazebo::transport::PublisherPtr customEventPub;
    
    /// \brief Button on flag
    private: bool buttonOn;
};
}
#endif
