/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2016, Andrei Haidu,
 *  Institute for Artificial Intelligence, Universität Bremen.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Institute for Artificial Intelligence,
 *     Universität Bremen, nor the names of its contributors may be
 *     used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "MagneticStirBarController.hh"
#include "GzUtils.hh"
#include <gazebo/transport/transport.hh>

using namespace sim_games;
using namespace gazebo;

// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(MagneticStirBarController)


//////////////////////////////////////////////////
MagneticStirBarController::MagneticStirBarController()
{
    this->buttonOn = false;
}

//////////////////////////////////////////////////
MagneticStirBarController::~MagneticStirBarController()
{
}

//////////////////////////////////////////////////
void MagneticStirBarController::Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
{
    // get my model
    this->model = _parent;
    
    // get the world
    this->world = _parent->GetWorld();

    // get the pid values from the sdf
    this->pidValues = GetSDFValue(std::string("pid"), _sdf, math::Vector3(1,0,0));
    
    // get the stirrer model name
    this->magneticStirrer = this->world->GetModel(GetSDFValue(std::string("stirrer_model"), _sdf, std::string("MagneticStirrer")));
    
    // Initialize the transport node
    this->gznode = transport::NodePtr(new transport::Node());
    this->gznode->Init(this->world->GetName());

    // Subscribe to the stirrer button
    this->buttonSub = this->gznode->Subscribe("~/magnetic_stirrer_button_contact",
            &MagneticStirBarController::OnButton, this);
    
    // Publish custom events
    this->customEventPub = this->gznode->Advertise<msgs::GzString>("~/custom_events");
    
    // set the initial desired position
    this->desiredPosition = this->magneticStirrer->GetWorldPose().pos + math::Vector3(0, 0, 0.03);
    
    // set the initial desired rot
    this->desiredQuaternion = _parent->GetWorldPose().rot;
    
    // set the PID values
    this->xPosPID = common::PID(this->pidValues.x, this->pidValues.y, this->pidValues.z, 10, -10);
    this->yPosPID = common::PID(this->pidValues.x, this->pidValues.y, this->pidValues.z, 10, -10);
    this->zPosPID = common::PID(this->pidValues.x, this->pidValues.y, this->pidValues.z, 10, -10);
    
    // listen to the update event, this event is broadcast every sim iteration.
    this->updateConnection = event::Events::ConnectWorldUpdateBegin(
            boost::bind(&MagneticStirBarController::OnUpdate, this));
    
    std::cout << "******** MAGNETIC STIR CONTROLLER PLUGIN LOADED *********" << std::endl;
}

/////////////////////////////////////////////////
void MagneticStirBarController::OnUpdate()
{   
    // check if stir bar is in the magnetic field
    if(this->model->GetWorldPose().pos.Distance(this->magneticStirrer->GetWorldPose().pos) < 0.085)
    {            
        // get curr ts
        const common::Time curr_ts = this->world->GetSimTime();

        // compute step time
        const common::Time step_time = curr_ts - this->prevTimestamp;

        // save prev ts
        this->prevTimestamp = curr_ts;

        // apply the control forces
        MagneticStirBarController::MagneticControl(step_time);
        
        // rotate the bar if the device is switched on
        if(this->buttonOn)
        {        
            this->desiredQuaternion *= math::Quaternion(0, 0.01, 0);
        }
    }
}

/////////////////////////////////////////////////
void MagneticStirBarController::OnButton(ConstContactsPtr &_msg)
{   
    if(_msg->contact_size() > 0)
    {        
        // toggle switch
        this->buttonOn = !this->buttonOn;
        if(this->buttonOn)
        {
            std::cout << "Magnetic stirrer: ON" << std::endl;
            
            if(util::LogRecord::Instance()->Running())
            {
                // create event msg
                msgs::GzString gzstr_msg;                
                gzstr_msg.set_data(std::string("TurningOnPoweredDevice,MagneticStirrer,")
                + std::to_string(this->world->GetSimTime().Double()));
                
                // publish the msg
                this->customEventPub->Publish(gzstr_msg);   
            }
        }
        else
        {
            std::cout << "Magnetic stirrer: OFF" << std::endl;
            
            if(util::LogRecord::Instance()->Running())
            {                
                // create event msg
                msgs::GzString gzstr_msg;                
                gzstr_msg.set_data(std::string("TurningOffPoweredDevice,MagneticStirrer,")
                + std::to_string(this->world->GetSimTime().Double()));
                
                // publish the msg
                this->customEventPub->Publish(gzstr_msg);
            }
        }
    }
}


/////////////////////////////////////////////////
void MagneticStirBarController::MagneticControl(const gazebo::common::Time _step_time)
{
    // get current pose of the model
    const math::Pose curr_pose = this->model->GetWorldPose();

    // computing PID effort values from the current position, for the x, y, z axis
    const math::Vector3 pos_effort = math::Vector3(
        this->xPosPID.Update(curr_pose.pos.x - this->desiredPosition.x, _step_time),
        this->yPosPID.Update(curr_pose.pos.y - this->desiredPosition.y, _step_time),
        this->zPosPID.Update(curr_pose.pos.z - this->desiredPosition.z, _step_time));

    // compute rot forces/vel 
    const math::Quaternion quat_diff = (this->desiredQuaternion - curr_pose.rot) * 3.0;
    const math::Quaternion vel_q = quat_diff * curr_pose.rot.GetInverse();    
    const math::Vector3 rot_vel = math::Vector3(vel_q.x * 8, vel_q.y * 8, vel_q.z * 8);
    
    // apply the computed lin/rot forces/velocities to the model
    this->model->GetLink("magnetic_stir_bar_link")->SetForce(pos_effort);
    this->model->SetAngularVel(rot_vel);
}