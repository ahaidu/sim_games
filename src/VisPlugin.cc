/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, Andrei Haidu, Institute for Artificial Intelligence,
 *  Universität Bremen.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Institute for Artificial Intelligence,
 *     Universität Bremen, nor the names of its contributors may be
 *     used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "VisPlugin.hh"

#define PI 3.14159265

using namespace sim_games;
using namespace gazebo;

// Register this plugin with the simulator
GZ_REGISTER_GUI_PLUGIN(VisPlugin)

//////////////////////////////////////////////////
VisPlugin::VisPlugin()
{
    // Set the frame background and foreground colors
    this->setStyleSheet(
        "QFrame { background-color : rgba(100, 100, 100, 255); color : white; }");
    
    // Create the main layout
    this->mainLayout = new QVBoxLayout;

    // Create the frame to hold all the widgets
    this->mainFrame = new QFrame();

    // Create the layout that sits inside the frame
    this->childLayout = new QGridLayout();    
    
     // Create text area
    this->txtArea = new QTextEdit(); 
    this->txtArea->setTextColor(QColor(255,250,240));
    this->txtArea->setText("Please execute the following task:");
    this->txtArea->setReadOnly(true);
    this->childLayout->addWidget(this->txtArea);
    
    // Create a push button to load the models
    this->loadModelsButton = new QPushButton(tr("Load Models"));
    connect(this->loadModelsButton, SIGNAL(clicked()), this, SLOT(OnLoadModelsButton()));
    
    // Add the button to the frame's layout
    this->childLayout->addWidget(this->loadModelsButton);
    
    
    // Create a push button, and connect it to the OnButton function
    this->viewLabelsButton = new QRadioButton(tr("View Labels"));
    this->viewLabelsButton->setChecked(true);
    connect(this->viewLabelsButton, SIGNAL(clicked()), this, SLOT(OnViewLabelsButton()));
    
    // Add the button to the frame's layout
    this->childLayout->addWidget(this->viewLabelsButton);
    
        
    
    // Add childLayout to the frame
    this->mainFrame->setLayout(childLayout);    

    // Add the frame to the main layout
    this->mainLayout->addWidget(this->mainFrame);

    // Remove margins to reduce space
    this->childLayout->setContentsMargins(0, 0, 0, 0);
    this->mainLayout->setContentsMargins(0, 0, 0, 0);

    // set main layout
    this->setLayout(this->mainLayout);
    
    
    // Position and resize this widget
    this->move(10, 10);
    this->resize(260, 210);
    
    // Create a node for transportation
    this->gznode = transport::NodePtr(new transport::Node());
    this->gznode->Init("chemlab_world");
    
    // publisher to spawn models
    this->factoryPub = this->gznode->Advertise<msgs::Factory>("~/factory");
    
    // subscribe to the prac messages
    this->pracSub = this->gznode->Subscribe<msgs::GzString>("~/prac_events",
        &VisPlugin::OnPracEventMsg, this);
            
    std::cout << "******** GUI VIS PLUGIN CONSTR *********" << std::endl;
}

//////////////////////////////////////////////////
VisPlugin::~VisPlugin()
{
}

//////////////////////////////////////////////////
void VisPlugin::Load(sdf::ElementPtr _sdf)
{
    // set scene manager
    this->sceneManager = gui::get_active_camera()->GetScene()->OgreSceneManager();
    
    // step once into the simulation in order to read the prac messages
    transport::PublisherPtr world_control_pub = this->gznode->Advertise<msgs::WorldControl>("~/world_control");
    world_control_pub->WaitForConnection();
    msgs::WorldControl world_control_msg;
    world_control_msg.set_multi_step(1);    
    world_control_pub->Publish(world_control_msg);
}

/////////////////////////////////////////////////
void VisPlugin::OnLoadModelsButton()
{    
    VisPlugin::SpawnModel("MagneticStirrer",
                          "model://magnetic_stirrer/magnetic_stirrer.sdf",
                          math::Pose(0.4,0.3,0.785,0,0,0) 
                         );
    
    VisPlugin::SpawnModel("Pipette",
                        "model://pipette/pipette.sdf",
                        math::Pose(0.4,-0.3,0.73,0,0,0) 
                        );
    
    VisPlugin::SpawnModel("ErlenmeyerFlask",
                        "model://erlenmeyer_flask/erlenmeyer_flask.sdf",
                        math::Pose(0.4,0.1,0.71,0,0,0) 
                        );
    
    VisPlugin::SpawnModel("MeasuringCylinder",
                        "model://measuring_cylinder/measuring_cylinder.sdf",
                        math::Pose(0.5,-0.1,0.71,0,0,0) 
                        );
    
    VisPlugin::SpawnModel("MagneticStirBar",
                        "model://magnetic_stir_bar/magnetic_stir_bar.sdf",
                        math::Pose(0.4,0.1,0.73,1.57,0,0) 
                        );
    
  
    VisPlugin::SpawnLiquid(VisPlugin::ToCamelCase(this->content), math::Pose(0.5,-0.1,0.73,0,0,0), "Gazebo/Red");
    
    // create label for the substance
    this->labels.push_back(
        VisPlugin::CreateLabel(this->content,math::Vector3(0.5,-0.1,0.73),
        common::Color::Red));

    
    VisPlugin::SpawnLiquid(VisPlugin::ToCamelCase(this->goal), math::Pose(0.4,0.1,0.74,0,0,0), "Gazebo/Blue");
    
    // create label for the substance
    this->labels.push_back(
        VisPlugin::CreateLabel(this->goal, math::Vector3(0.4,0.1,0.74),
        common::Color::Blue));
    
    // disable button
    this->loadModelsButton->setEnabled(false);
}

/////////////////////////////////////////////////
void VisPlugin::OnViewLabelsButton()
{
    for (auto l : labels)
    {
        l->flipVisibility();
    }
}

/////////////////////////////////////////////////
void VisPlugin::OnPracEventMsg(ConstGzStringPtr& _msg)
{ 
    // get the text from the xml doc
    std::stringstream ev_ss(_msg->data());
    
    // vector of the items
    std::vector<std::string> ev_items;
    std::string ev_item;
    
    // add the event items to the vector
    while(std::getline(ev_ss, ev_item, ',')) 
    {
        ev_items.push_back(ev_item);
    }
    
    const std::string ev_1 = ev_items.at(1);
    const std::string ev_2 = ev_items.at(2);
    
    if (ev_1 == "instruction")
    {
        this->txtArea->append(QString::fromStdString("<font color=\"#F0E68C\">" + ev_2));
        this->txtArea->append(QString(""));
    }
    else if(ev_1 == "content")
    {        
        this->content = ev_2;
        this->txtArea->append(QString::fromStdString(
            ev_1 + " : <font color=\"Red\">" + ev_2));
    }
    else if(ev_1 == "goal")
    {
        this->goal = ev_2;
        this->txtArea->append(QString::fromStdString(
            ev_1 + " : <font color=\"Blue\">" + ev_2));
    }
    else
    {
        this->txtArea->append(QString::fromStdString(
            ev_1 + " : <font color=\"#F0E68C\">" + ev_2));
    }
}

/////////////////////////////////////////////////
void VisPlugin::SpawnModel(const std::string _name, const std::string _uri, const math::Pose _pose)
{   
    // create an sdf
    sdf::SDFPtr sdf(new sdf::SDF());
    if (!sdf::init(sdf))
    {
      std::cout << "Error: SDF parsing the xml failed" << std::endl;
    }

    // get the file path using the uri
    if (!sdf::readFile(common::SystemPaths::Instance()->FindFileURI(_uri), sdf))
    {
      std::cout << "Error: SDF parsing the xml failed, uri: " << _uri << std::endl;
    }
    
    // get the model element from the sdf
    sdf::ElementPtr model_elem = sdf->Root()->GetElement("model");
    if (!model_elem)
    {
        std::cout << "Unable to find <model> element, for sdf: " << _uri << std::endl;
    }
    
    // Set the model name
    if (!_name.empty())
    {
        model_elem->GetAttribute("name")->SetFromString(_name);
    }
    
    // create and publish the factory msg
    factoryPub->WaitForConnection();
    msgs::Factory factory_msg;
    factory_msg.set_sdf(sdf->ToString());
    msgs::Set(factory_msg.mutable_pose(), _pose.Ign());
    factoryPub->Publish(factory_msg, true);
}

/////////////////////////////////////////////////
void VisPlugin::SpawnLiquid(const std::string _name, const math::Pose _pose, 
                            const std::string _color)
{  
    // create and publish the factory msg
    factoryPub->WaitForConnection();

    // create sdf of the liquid
    std::string liquid_sdf_str(
        VisPlugin::GenerateSDF(_name, "sphere", "sphere", 25, 0.02, _pose.pos,
                               math::Vector3(0.0000001,0.0000001,0.0000001), 0.00005, 0.005,
                               1.0, 0.00000002, 100000000.8, 1.2, "0x03",
                               "file://media/materials/scripts/gazebo.material", "", "",
                               1.0, _color, false));

    // create and publish the factory msg
    msgs::Factory factory_msg;
    factory_msg.set_sdf(liquid_sdf_str);
    this->factoryPub->Publish(factory_msg);
}

/////////////////////////////////////////////////
Ogre::SceneNode* VisPlugin::CreateLabel(const std::string _name, const math::Vector3 _pos,
    const common::Color _color)
{   
    // create movable text
    rendering::MovableText *movable_text;
    movable_text = new rendering::MovableText();
    movable_text->Load(_name, _name, "Arial", 0.02, _color);
    movable_text->SetShowOnTop(true);
    
    // create and attach to scene node
    Ogre::SceneNode *movable_text_node =  this->sceneManager->getRootSceneNode()->createChildSceneNode(_name + "_scene_node");    
    movable_text_node->attachObject(movable_text);    
    movable_text_node->setInheritScale(false);
    movable_text_node->setPosition(_pos.x, _pos.y, _pos.z);
    
    // return scene node
    return movable_text_node;                                
}

//////////////////////////////////////////////////
std::string VisPlugin::GenerateSDF(
        const std::string _model_name,
        const std::string _spawn_geometry,
        const std::string _vis_geometry,
        const double _unit_nr,
        const double _spawn_diam,
        const math::Vector3 _spawn_pos,
        const math::Vector3 _inertia_vect,
        const double _unit_mass,
        const double _unit_size,
        const double _cfm,
        const double _erp,
        const double _kp,
        const double _kd,
        const std::string _collide_bitmask,
        const std::string _scripts_uri,
        const std::string _textures_uri,
        const std::string _mesh_uri,
        const double _mesh_scale,
        const std::string _script_name,
        const bool _static_flag
        )
{
    std::stringstream sdf_ss;

    // positioning of the current sphere
    math::Vector3 p3;

    // currently spawned spheres
    int spawned = 0;

    // current height level
    int level = 0;


    sdf_ss << "<?xml version='1.0'?>\n";
    sdf_ss << "<sdf version='1.5'>\n";
    sdf_ss << "<model name="<< _model_name <<">\n";
    sdf_ss << "\t<pose>" << _spawn_pos.x << " " << _spawn_pos.y << " " << _spawn_pos.z << " 0 0 0 </pose>\n";

    // Loop for every sphere
    for (unsigned int i = 0; i < _unit_nr; i++)
    {
        // returns the position of the current sphere, spawned and level are returned by reference
        p3 = VisPlugin::ArrangeUnits(i, _unit_size, _spawn_diam, spawned, level);

        sdf_ss << "\t\t<link name='" << _model_name << "_link_" << i << "'>\n";
        sdf_ss << "\t\t\t<self_collide>true</self_collide>\n";
        sdf_ss << "\t\t\t<pose>" << p3.x << " " << p3.y << " " << p3.z << " 0 0 0</pose>\n";

        sdf_ss << "\t\t\t<inertial>\n";
        sdf_ss << "\t\t\t\t<pose> 0 0 0 0 0 0 </pose>\n";
        sdf_ss << "\t\t\t\t<inertia>\n";
        sdf_ss << "\t\t\t\t\t<ixx>" << _inertia_vect.x << "</ixx>\n";
        sdf_ss << "\t\t\t\t\t<ixy>0</ixy>\n";
        sdf_ss << "\t\t\t\t\t<ixz>0</ixz>\n";
        sdf_ss << "\t\t\t\t\t<iyy>" << _inertia_vect.y << "</iyy>\n";
        sdf_ss << "\t\t\t\t\t<iyz>0</iyz>\n";
        sdf_ss << "\t\t\t\t\t<izz>" << _inertia_vect.z << "</izz>\n";
        sdf_ss << "\t\t\t\t</inertia>\n";
        sdf_ss << "\t\t\t\t<mass>" << _unit_mass << "</mass>\n";
        sdf_ss << "\t\t\t</inertial>\n";

        sdf_ss << "\t\t\t<collision name='" << _model_name << "_collision_" << i << "'>\n";
        sdf_ss << "\t\t\t\t<geometry>\n";
            if(_spawn_geometry == "sphere")
            {
                sdf_ss << "\t\t\t\t\t<sphere>\n";
                sdf_ss << "\t\t\t\t\t\t<radius>" << _unit_size << "</radius>\n";
                sdf_ss << "\t\t\t\t\t</sphere>\n";
            }
            else if(_spawn_geometry == "box")
            {
                sdf_ss << "\t\t\t\t\t<box>\n";
                sdf_ss << "\t\t\t\t\t\t<size>" << _unit_size << " " << _unit_size <<  " " << _unit_size << "</size>\n";
                sdf_ss << "\t\t\t\t\t</box>\n";
            }
            else if(_spawn_geometry == "cylinder")
            {
                sdf_ss << "\t\t\t\t\t<cylinder>\n";
                sdf_ss << "\t\t\t\t\t\t<radius>" << _unit_size << "</radius>\n";
                sdf_ss << "\t\t\t\t\t\t<length>" << _unit_size / 3 << "</length>\n";
                sdf_ss << "\t\t\t\t\t</cylinder>\n";
            }
            else if(_spawn_geometry == "mesh")
            {
                sdf_ss << "\t\t\t\t\t<mesh>\n";
                sdf_ss << "\t\t\t\t\t\t<uri>" << _mesh_uri << "</uri>\n";
                sdf_ss << "\t\t\t\t\t\t<scale>" << _mesh_scale << " " << _mesh_scale <<  " " << _mesh_scale << "</scale>\n";
                sdf_ss << "\t\t\t\t\t</mesh>\n";
            }
        sdf_ss << "\t\t\t\t</geometry>\n";


        sdf_ss << "\t\t\t\t<surface>\n";

        // TODO reimplement the sdf parameters as well
        // xml << "\t\t\t\t\t<bounce>\n";
        // xml << "\t\t\t\t\t\t<restitution_coefficient>" << bounce << "</restitution_coefficient>\n";
        // xml << "\t\t\t\t\t\t<threshold>10000.0</threshold>\n";
        // xml << "\t\t\t\t\t</bounce>\n";

        // xml << "\t\t\t\t\t<friction>\n";
        // xml << "\t\t\t\t\t\t<ode>\n";
        // xml << "\t\t\t\t\t\t\t<mu>" << mu << "</mu>\n";
        // xml << "\t\t\t\t\t\t\t<mu2>" << mu2 << "</mu2>\n";
        // xml << "\t\t\t\t\t\t\t<fdir1>0.0 0.0 0.0</fdir1>\n";
        // xml << "\t\t\t\t\t\t\t<slip1>" << slip1 << "</slip1>\n";
        // xml << "\t\t\t\t\t\t\t<slip2>" << slip2 << "</slip2>\n";
        // xml << "\t\t\t\t\t\t</ode>\n";
        // xml << "\t\t\t\t\t\t<bullet>\n";
        // xml << "\t\t\t\t\t\t\t<friction>" << friction << "</friction>\n";
        // xml << "\t\t\t\t\t\t\t<friction2>" << friction2 << "</friction2>\n";
        // xml << "\t\t\t\t\t\t\t<rolling_friction>" << roll_friction << "</rolling_friction>\n";
        // xml << "\t\t\t\t\t\t</bullet>\n";
        // xml << "\t\t\t\t\t</friction>\n";

        sdf_ss << "\t\t\t\t\t<contact>\n";
//        sdf_ss << "\t\t\t\t\t\t<ode>\n";
//        sdf_ss << "\t\t\t\t\t\t\t<soft_cfm>" << _cfm << "</soft_cfm>\n";
//        sdf_ss << "\t\t\t\t\t\t\t<soft_erp>" << _erp << "</soft_erp>\n";
//        sdf_ss << "\t\t\t\t\t\t\t<kp>" << _kp << "</kp>\n";
//        sdf_ss << "\t\t\t\t\t\t\t<kd>" << _kd << "</kd>\n";
//        sdf_ss << "\t\t\t\t\t\t\t<max_vel>100.0</max_vel>\n";
//        sdf_ss << "\t\t\t\t\t\t\t<min_depth>0.001</min_depth>\n";
//        sdf_ss << "\t\t\t\t\t\t</ode>\n";
        sdf_ss <<  "\t\t\t\t\t\t<collide_bitmask>" << _collide_bitmask << "</collide_bitmask>\n";
        sdf_ss << "\t\t\t\t\t</contact>\n";

        sdf_ss << "\t\t\t\t</surface>\n";
        sdf_ss << "\t\t\t</collision>\n";

        sdf_ss << "\t\t\t<visual name='" << _model_name << "_visual_" << i << "'>\n";
        sdf_ss << "\t\t\t\t<geometry>\n";
            if(_vis_geometry == "sphere")
            {
                sdf_ss << "\t\t\t\t\t<sphere>\n";
                sdf_ss << "\t\t\t\t\t\t<radius>" << _unit_size << "</radius>\n";
                sdf_ss << "\t\t\t\t\t</sphere>\n";
            }
            else if(_vis_geometry == "box")
            {
                sdf_ss << "\t\t\t\t\t<box>\n";
                sdf_ss << "\t\t\t\t\t\t<size>" << _unit_size << " " << _unit_size <<  " " << _unit_size << "</size>\n";
                sdf_ss << "\t\t\t\t\t</box>\n";
            }
            else if(_vis_geometry == "cylinder")
            {
                sdf_ss << "\t\t\t\t\t<cylinder>\n";
                sdf_ss << "\t\t\t\t\t\t<radius>" << _unit_size << "</radius>\n";
                sdf_ss << "\t\t\t\t\t\t<length>" << _unit_size / 3 << "</length>\n";
                sdf_ss << "\t\t\t\t\t</cylinder>\n";
            }
            else if(_vis_geometry == "mesh")
            {
                sdf_ss << "\t\t\t\t\t<mesh>\n";
                sdf_ss << "\t\t\t\t\t\t<uri>" << _mesh_uri << "</uri>\n";
                sdf_ss << "\t\t\t\t\t\t<scale>" << _mesh_scale << " " << _mesh_scale <<  " " << _mesh_scale << "</scale>\n";
                sdf_ss << "\t\t\t\t\t</mesh>\n";
            }
        sdf_ss << "\t\t\t\t</geometry>\n";

        if(_scripts_uri != "")
        {
            sdf_ss << "\t\t\t\t<material>\n";
            sdf_ss << "\t\t\t\t\t<script>\n";
            sdf_ss << "\t\t\t\t\t\t<uri>" << _scripts_uri << "</uri>\n";
            if(_textures_uri != "")
            {
                sdf_ss << "\t\t\t\t\t\t<uri>" << _textures_uri << "</uri>\n";
            }
            sdf_ss << "\t\t\t\t\t\t<name>" << _script_name << "</name>\n";
            sdf_ss << "\t\t\t\t\t</script>\n";
            sdf_ss << "\t\t\t\t</material>\n";
        }

        sdf_ss << "\t\t\t</visual>\n";
        sdf_ss << "\t\t</link>\n";
    }

    sdf_ss << "<allow_auto_disable>false</allow_auto_disable>\n";
    sdf_ss << "<static>" << _static_flag << "</static>\n";
    sdf_ss << "</model>\n";
    sdf_ss << "</sdf>\n";
    
    return sdf_ss.str();
}

//////////////////////////////////////////////////
math::Vector3 VisPlugin::ArrangeUnits(
        const int _curr_sphere_index,
        const double _sphere_radius,
        const double _spawn_diameter,
        int& _spawned,
        int& level)
{
    math::Vector3 v3;
    int ii, index_c, c_crt, max_c_in_c;
    double size, R;
    ii = _curr_sphere_index - _spawned;
    size = _sphere_radius * 2;

    v3.z = level * size;
    if (ii != 0)
    {
        index_c = ii-1;
        c_crt = 1;

        while (index_c >= (6*c_crt))
        {
            index_c -= 6*c_crt;
            c_crt++;
        }
        max_c_in_c = c_crt * 6;
        R = c_crt * size;

        if((index_c == (max_c_in_c - 1)) && ((2*R) + (size) >= _spawn_diameter))
        {
            _spawned = _curr_sphere_index+1;
            level++;
        }

        v3.x = R * cos((double) index_c * 2 * PI / max_c_in_c);
        v3.y = R * sin((double) index_c * 2 * PI / max_c_in_c);
    }

    return v3;
}

/////////////////////////////////////////////////
std::string VisPlugin::ToCamelCase(const std::string _name)
{
    // change name to stringstream
    std::stringstream full_name_ss(_name);
        
    // return
    std::string camel_case;
    
    // vectors after tokenization
    std::vector<std::string> dot_items;
    std::vector<std::string> underscore_items;    
    std::string curr_item;
    
    // tokenize with dots
    while(std::getline(full_name_ss, curr_item, '.')) 
    {
        dot_items.push_back(curr_item);
    }
    
    // change name to stringstream
    std::stringstream underscore_name_ss(dot_items.at(0));
    
    // tokenize with underscore
    while(std::getline(underscore_name_ss, curr_item, '_'))
    {
        // uppercase the item and add it to the vector
        curr_item[0] = std::toupper(curr_item[0]);
        underscore_items.push_back(curr_item);
    }
    
    // append the items
    for(auto s : underscore_items)
    {
        camel_case+=s;
    }
    
    return camel_case;
}















