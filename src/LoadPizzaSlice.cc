/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, Andrei Haidu,
 *  Institute for Artificial Intelligence, Universität Bremen.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Institute for Artificial Intelligence,
 *     Universität Bremen, nor the names of its contributors may be
 *     used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "LoadPizzaSlice.hh"
#include "GzUtils.hh"

using namespace gazebo;
using namespace sim_games;


// Register this plugin with the simulator
GZ_REGISTER_WORLD_PLUGIN(LoadPizzaSlice)


//////////////////////////////////////////////////
LoadPizzaSlice::LoadPizzaSlice()
{
}

//////////////////////////////////////////////////
LoadPizzaSlice::~LoadPizzaSlice()
{

}

//////////////////////////////////////////////////
void LoadPizzaSlice::Load(physics::WorldPtr _parent, sdf::ElementPtr _sdf)
{
    // spawn pos
    const math::Vector3 spawn_pos =
            GetSDFValue(std::string("pos"), _sdf, math::Vector3(0,0,0));

    // X length
    const double length_x =
            GetSDFValue(std::string("length_x"), _sdf, 0.1);

    // Y length
    const double length_y =
            GetSDFValue(std::string("length_y"), _sdf, 0.1);

    // X nr
    const unsigned int nr_x =
            GetSDFValue(std::string("nr_x"), _sdf, 2);

    // y nr
    const unsigned int nr_y =
            GetSDFValue(std::string("nr_y"), _sdf, 2);

    // geometry type
    const std::string geometry =
            GetSDFValue(std::string("geometry"), _sdf, std::string("sphere"));

    // inertia vect
    const math::Vector3 inertia_vect =
            GetSDFValue(std::string("inertia_v"), _sdf, math::Vector3(0.1, 0.1, 0.1));

    // unit mass
    const double unit_mass =
            GetSDFValue(std::string("unit_mass"), _sdf, 0.05);

    // unit size
    const double unit_size =
            GetSDFValue(std::string("unit_size"), _sdf, 0.01);

    // joint cfm
    const double joint_cfm =
            GetSDFValue(std::string("joint_cfm"), _sdf, 0.8);

    // joint erp
    const double joint_erp =
            GetSDFValue(std::string("joint_erp"), _sdf, 0.2);

//    std::cout << spawn_pos << std::endl;
//    std::cout << length_x << std::endl;
//    std::cout << length_y << std::endl;
//    std::cout << nr_x << std::endl;
//    std::cout << nr_y << std::endl;
//    std::cout << geometry << std::endl;
//    std::cout << inertia_vect << std::endl;
//    std::cout << unit_mass << std::endl;
//    std::cout << unit_size << std::endl;
//    std::cout << joint_cfm << std::endl;
//    std::cout << joint_erp << std::endl;

    // generate the pizza sdf
    sdf::SDF pizzaSliceSDF = LoadPizzaSlice::GeneratePizzaSliceSDF(
                spawn_pos, length_x, length_y, nr_x, nr_y, geometry, inertia_vect,
                unit_mass, unit_size, joint_cfm, joint_erp);

    // Insert generated sdf into the world
    _parent->InsertModelSDF(pizzaSliceSDF);

    std::cout << "******** PIZZA SLICE SPAWNED *********" << std::endl;
}

//////////////////////////////////////////////////
sdf::SDF LoadPizzaSlice::GeneratePizzaSliceSDF(
        const math::Vector3 _pos,
        const double _length_x,
        const double _length_y,
        const double _nr_x,
        const double _nr_y,
        const std::string _geometry,
        const math::Vector3 _inertia_v,
        const double _unit_mass,
        const double _unit_size,
        const double _cfm,
        const double _erp)
{
    // sdf stringstream
    std::stringstream sdf_ss;

    // joints string
    std::string joints_str;

    // position of the first unit, left up corner, half of the total size
    const double corner_pos_x = _length_x / 2;
    const double corner_pos_y = _length_y / 2;

    // print info in case of possibility of nan numbers
    if (_nr_x <= 1 || _nr_y <= 1)
    {
        std::cout << "**LoadPizzaSlice** nr_y and nr_z need to be larger than 1!" << std::endl;
    }

    // dist between units on x and y axis
    const double unit_dist_x = _length_x / (_nr_x - 1);
    const double unit_dist_y = _length_y / (_nr_y - 1);

    // sart the sdf of the pizza slice
    sdf_ss << "<?xml version='1.0'?>\n";
    sdf_ss << "<sdf version='1.5'>\n";
    sdf_ss << "<model name=PizzaSlice>\n";
    sdf_ss << "\t<pose>" << _pos.x << " " << _pos.y << " " << _pos.z << " 0 0 0 </pose>\n";

    // current unit iter
    unsigned int curr_it = 1;

    // iterate on x axis units
    for (unsigned int i = 0; i < _nr_x; ++i)
    {
        // iterate on y axis units
        for (unsigned int j = 0; j < _nr_y; ++j)
        {
            // relative pos of the unit
            const math::Vector3 link_pos = math::Vector3(corner_pos_x - (i * unit_dist_x),
                                                   corner_pos_y - (j * unit_dist_y),
                                                   0);

            // create current link
            sdf_ss << LoadPizzaSlice::GenerateLink(
                          link_pos,_geometry, _inertia_v, _unit_mass, _unit_size, curr_it);

            // if we are at the first row, and past the first value,
            // create joints with the prev links
            if(i == 0 && j > 0)
            {
                joints_str += LoadPizzaSlice::GenerateJoint(
                            math::Vector3(0, unit_dist_y/2, 0), curr_it - 1, curr_it, _cfm, _erp);
            }
            // case of new row and first link,
            // creating joint between the current links and the first links from the prev row
            else if(i > 0 && j == 0)
            {
                joints_str += LoadPizzaSlice::GenerateJoint(
                            math::Vector3(unit_dist_x/2, 0, 0), curr_it - _nr_y, curr_it, _cfm, _erp);
            }
            // case of new row, different from the first link,
            // creating joint betweeen the curr link and the prev one, and the one from the prev row
            else if(i > 0 && j > 0)
            {
                joints_str += LoadPizzaSlice::GenerateJoint(
                            math::Vector3(0, unit_dist_y/2, 0), curr_it - 1, curr_it, _cfm, _erp);

                joints_str += LoadPizzaSlice::GenerateJoint(
                            math::Vector3(unit_dist_x/2, 0, 0), curr_it - _nr_y, curr_it, _cfm, _erp);
            }

            // increment unit iter
            ++curr_it;
        }
    }

    // add joints ss to the sdf
    sdf_ss << joints_str;



    // end pizza slice sdf ss
    sdf_ss << "<allow_auto_disable>false</allow_auto_disable>\n";
    sdf_ss << "<static>false</static>\n";
    sdf_ss << "</model>\n";
    sdf_ss << "</gazebo>\n";

//    std::cout << sdf_ss.str() << "\n";

    sdf::SDF pizzaSliceSDF;
    pizzaSliceSDF.SetFromString(sdf_ss.str());

    return pizzaSliceSDF;
}

//////////////////////////////////////////////////
std::string LoadPizzaSlice::GenerateLink(
        const gazebo::math::Vector3 _link_pos,
        const std::string _geometry,
        const gazebo::math::Vector3 _inertia_v,
        const double _unit_mass,
        const double _unit_size,
        const unsigned int _it)
{
    // sdf stringstream
    std::stringstream link_ss;

    link_ss << "\t\t<link name='pizza_slice_link_" << _it <<"'>\n";
    link_ss << "\t\t\t<pose>" << _link_pos.x << " " << _link_pos.y << " 0 0 0 0</pose>\n";

    link_ss << "\t\t\t<inertial>\n";
    link_ss << "\t\t\t\t<inertia>\n";
    link_ss << "\t\t\t\t\t<ixx>" << _inertia_v.x << "</ixx>\n";
    link_ss << "\t\t\t\t\t<iyy>" << _inertia_v.y << "</iyy>\n";
    link_ss << "\t\t\t\t\t<izz>" << _inertia_v.z << "</izz>\n";
    link_ss << "\t\t\t\t</inertia>\n";
    link_ss << "\t\t\t\t<mass>" << _unit_mass << "</mass>\n";
    link_ss << "\t\t\t</inertial>\n";

    link_ss << "\t\t\t<collision name='pizza_slice_collision_" << _it <<"'>\n";
    link_ss << "\t\t\t\t<geometry>\n";
    if(_geometry == "sphere")
    {
        link_ss << "\t\t\t\t\t<sphere>\n";
        link_ss << "\t\t\t\t\t\t<radius>" << _unit_size << "</radius>\n";
        link_ss << "\t\t\t\t\t</sphere>\n";
    }
    else if(_geometry == "box")
    {
        link_ss << "\t\t\t\t\t<box>\n";
        link_ss << "\t\t\t\t\t\t<size>" << _unit_size << " " << _unit_size <<  " " << _unit_size << "</size>\n";
        link_ss << "\t\t\t\t\t</box>\n";
    }
    else if(_geometry == "cylinder")
    {
        link_ss << "\t\t\t\t\t<cylinder>\n";
        link_ss << "\t\t\t\t\t\t<radius>" << _unit_size << "</radius>\n";
        link_ss << "\t\t\t\t\t\t<length>" << _unit_size << "</length>\n";
        link_ss << "\t\t\t\t\t</cylinder>\n";
    }
    link_ss << "\t\t\t\t</geometry>\n";
    link_ss << "\t\t\t</collision>\n";

    link_ss << "\t\t\t<visual name='pizza_slice_visual_" << _it <<"'>\n";
    link_ss << "\t\t\t\t<geometry>\n";
    if(_geometry == "sphere")
    {
        link_ss << "\t\t\t\t\t<sphere>\n";
        link_ss << "\t\t\t\t\t\t<radius>" << _unit_size << "</radius>\n";
        link_ss << "\t\t\t\t\t</sphere>\n";
    }
    else if(_geometry == "box")
    {
        link_ss << "\t\t\t\t\t<box>\n";
        link_ss << "\t\t\t\t\t\t<size>" << _unit_size << " " << _unit_size <<  " " << _unit_size << "</size>\n";
        link_ss << "\t\t\t\t\t</box>\n";
    }
    else if(_geometry == "cylinder")
    {
        link_ss << "\t\t\t\t\t<cylinder>\n";
        link_ss << "\t\t\t\t\t\t<radius>" << _unit_size << "</radius>\n";
        link_ss << "\t\t\t\t\t\t<length>" << _unit_size << "</length>\n";
        link_ss << "\t\t\t\t\t</cylinder>\n";
    }
    link_ss << "\t\t\t\t</geometry>\n";
    link_ss << "\t\t\t\t<material>\n";
    link_ss << "\t\t\t\t\t<script>\n";
    link_ss << "\t\t\t\t\t\t<uri>file://media/materials/scripts/gazebo.material</uri>\n";
    link_ss << "\t\t\t\t\t\t<name>Gazebo/Red</name>\n";
    link_ss << "\t\t\t\t\t</script>\n";
    link_ss << "\t\t\t\t</material>\n";
    link_ss << "\t\t\t</visual>\n";

    link_ss << "\t\t</link>\n";

    return link_ss.str();
}


//////////////////////////////////////////////////
std::string LoadPizzaSlice::GenerateJoint(
        const gazebo::math::Vector3 _joint_pos,
        const unsigned int _parent_it,
        const unsigned int _child_it,
        const double _cfm,
        const double _erp)
{
    // sdf stringstream
    std::stringstream joint_ss;

    // the joint pose is relative to the child
    joint_ss << "\t\t<joint name='pizza_slice_j_" << _parent_it << "_" << _child_it
             <<"' type='revolute'>\n";
    joint_ss << "\t\t\t<pose>" << _joint_pos.x << " " << _joint_pos.y << " 0 0 0 0</pose>\n";
    joint_ss << "\t\t\t<parent>pizza_slice_link_" << _parent_it << "</parent>\n";
    joint_ss << "\t\t\t<child>pizza_slice_link_" << _child_it << "</child>\n";

    joint_ss << "\t\t\t<axis>\n";
    joint_ss << "\t\t\t\t<limit>\n";
    joint_ss << "\t\t\t\t\t<upper>0</upper>\n";
    joint_ss << "\t\t\t\t\t<lower>0</lower>\n";
    joint_ss << "\t\t\t\t</limit>\n";
    joint_ss << "\t\t\t\t<xyz>1 0 0</xyz>\n";
    joint_ss << "\t\t\t</axis>\n";

    joint_ss << "\t\t\t<physics>\n";
    joint_ss << "\t\t\t\t<ode>\n";
    joint_ss << "\t\t\t\t\t<cfm>" << _cfm << "</cfm>\n";
    joint_ss << "\t\t\t\t\t<erp>" << _erp << "</erp>\n";
    joint_ss << "\t\t\t\t</ode>\n";
    joint_ss << "\t\t\t</physics>\n";

    joint_ss << "\t\t</joint>\n";

    return joint_ss.str();
}
