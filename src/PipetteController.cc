/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2016, Andrei Haidu,
 *  Institute for Artificial Intelligence, Universität Bremen.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Institute for Artificial Intelligence,
 *     Universität Bremen, nor the names of its contributors may be
 *     used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "PipetteController.hh"
#include "GzUtils.hh"
#include <gazebo/transport/transport.hh>
#include "gazebo/util/LogRecord.hh"

using namespace sim_games;
using namespace gazebo;

// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(PipetteController)


//////////////////////////////////////////////////
PipetteController::PipetteController()
{
    this->buttonInContact = false;
    this->buttonPressedForDetach = false;
    this->jointAttached = false;
}

//////////////////////////////////////////////////
PipetteController::~PipetteController()
{
}

//////////////////////////////////////////////////
void PipetteController::Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
{
    // liquid model names
    std::string liquid_names =
            GetSDFValue(std::string("liquid_names"), _sdf, std::string(""));
    
    // add the liquid model names to the set
    while(!liquid_names.empty())
    {
        const std::string curr_liq_name = liquid_names.substr(0,liquid_names.find(","));
        this->liquidNames.insert(curr_liq_name);
        liquid_names.erase(0, curr_liq_name.size()+1);
    }
            
    // Store the pointer to the model
    this->pipetteModel = _parent;

    // initialize world
    this->world = this->pipetteModel->GetWorld();

    // Initialize the transport node
    this->gznode = transport::NodePtr(new transport::Node());

    this->gznode->Init(this->world->GetName());

    // Subscribe to the button contact sensor
    this->buttonContactSub = this->gznode->Subscribe("~/pipette_button_contact",
            &PipetteController::OnButtonContact, this);

    // Subscribe to the tip contact sensor
    this->tipContactSub = this->gznode->Subscribe("~/pipette_tip_contact",
            &PipetteController::OnTipContact, this);
    
    // Publish custom event msgs
    this->customEventPub = this->gznode->Advertise<msgs::GzString>("~/custom_events");
    
    std::cout << "******** PIPETTE PLUGIN LOADED *********" << std::endl;
}

//////////////////////////////////////////////////
void PipetteController::OnButtonContact(ConstContactsPtr &_msg)
{
    if(_msg->contact_size() > 0)
    {        
        // set contact flag
        this->buttonInContact = true;

        // detach joint
        if(this->jointAttached && this->buttonPressedForDetach)
        {            
            if(util::LogRecord::Instance()->Running())
            {                
                // create event msg
                msgs::GzString gzstr_msg;                
                gzstr_msg.set_data(std::string("Pipetting") + std::string(",")
                + this->holdModelName + std::string(",")
                + this->holdLinkName + std::string(",")
                + std::to_string(this->pipettingStartTs) + std::string(",")
                + std::to_string(this->world->GetSimTime().Double()));
                
                // publish the msg
                this->customEventPub->Publish(gzstr_msg);
            }            
            // release liquid from pipette 
            PipetteController::DetachJoint();
            this->buttonPressedForDetach = false;
        }
    }
    else
    {
        if(this->jointAttached)
        {
            this->buttonPressedForDetach = true;
        }
        this->buttonInContact = false;
    }
}

//////////////////////////////////////////////////
void PipetteController::OnTipContact(ConstContactsPtr &_msg)
{
    if(_msg->contact_size() > 0 && this->buttonInContact && !this->jointAttached)
    {
        // iterate until a contact with an allowed liquid is found
        for (unsigned int i = 0; i < _msg->contact_size(); ++i)
        {
            // get the first collision and its corresponding model and link name
            std::string c1 = _msg->contact(0).collision1();        
            // get the model name
            const std::string m1 = c1.substr(0,c1.find("::"));        
            // remove the model name from the string
            c1.erase(0, m1.length()+2);        
            // get the link name
            const std::string l1 = c1.substr(0,c1.find("::"));
                        
            // attach joint if the tip is in contact with an allowed liquid
            if(this->liquidNames.find(m1) != this->liquidNames.end())
            {
                if(util::LogRecord::Instance()->Running())
                {
                    // save the model and link name
                    this->holdLinkName = l1;
                    this->holdModelName = m1;
                    this->pipettingStartTs = this->world->GetSimTime().Double();
                }                
                PipetteController::AttachJoint(m1, l1);
                return;
            }  
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////
void PipetteController::AttachJoint(const std::string _model_name, const std::string _link_name)
{
    // get first link of the grasped model
     const physics::LinkPtr attach_link =
             this->world->GetModel(_model_name)->GetLink(_link_name);

    const physics::LinkPtr pipette_link =
            this->pipetteModel->GetLink("pipette_link");

    std::cout << "Creating pipette joint between " << pipette_link->GetName().c_str()
            << " and " << attach_link->GetName().c_str() << std::endl;
            
    const math::Pose offset(0,0,0,0,0,0);
            
    // move pipetted particle to the tip of the pipette
    attach_link->SetWorldPose(pipette_link->GetCollision("pipette_tip_collision")->GetWorldPose());

    // creating joint
    this->fixedJoint = this->world->GetPhysicsEngine()->CreateJoint(
                "revolute", this->pipetteModel);

    // attaching and setting the joint to fixed
    this->fixedJoint->Load(pipette_link, attach_link, math::Pose());
    this->fixedJoint->Init();
    this->fixedJoint->SetHighStop(0, 0);
    this->fixedJoint->SetLowStop(0, 0);

    // set attached flag to true
    this->jointAttached = true;
}

//////////////////////////////////////////////////////////////////////////////////////
void PipetteController::DetachJoint()
{
    std::cout << "Detaching Pipette Fixed Joint! " <<  std::endl;

    // removing and detaching joint
    this->fixedJoint->Reset(); //TODO check if needed
    this->fixedJoint->Detach();
    this->fixedJoint->Fini();

    // set attached flag to false
    this->jointAttached = false;
}