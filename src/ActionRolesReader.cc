/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2016, Andrei Haidu, Institute for Artificial Intelligence,
 *  Universität Bremen.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Institute for Artificial Intelligence,
 *     Universität Bremen, nor the names of its contributors may be
 *     used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "ActionRolesReader.hh"
#include <gazebo/transport/transport.hh>

using namespace gazebo;
using namespace sim_games;

// Register this plugin with the simulator
GZ_REGISTER_SYSTEM_PLUGIN(ActionRolesReader)

//////////////////////////////////////////////////
ActionRolesReader::ActionRolesReader()
{
}

//////////////////////////////////////////////////
ActionRolesReader::~ActionRolesReader()
{
}

//////////////////////////////////////////////////
void ActionRolesReader::Load(int _argc, char ** _argv)
{    
    // get the arguments from the terminal
    for (unsigned int i = 0; i < _argc; ++i)
    {
        if(std::string(_argv[i]) == "--instruction")
        {
            this->instruction = _argv[++i];
        }
        else if(std::string(_argv[i]) == "--ac")
        {   
           this->actionCore = _argv[++i];
        }
        else if(std::string(_argv[i]) == "--content")
        {   
           this->content = _argv[++i];
        }
        else if(std::string(_argv[i]) == "--stuff")
        {   
           this->stuff = _argv[++i];
        }        
        else if(std::string(_argv[i]) == "--goal")
        {   
           this->goal = _argv[++i];
        }
        else if(std::string(_argv[i]) == "--amount")
        {   
           this->amount = _argv[++i];
        }
        else if(std::string(_argv[i]) == "--unit")
        {   
           this->unit = _argv[++i];
        }
        else if(std::string(_argv[i]) == "--action_verb")
        {   
           this->actionVerb = _argv[++i];
        }
    }
    
    std::cout << "*ActionRolesReader: - instruction: " << this->instruction << std::endl;
    std::cout << "*ActionRolesReader: - action_core: " << this->actionCore << std::endl;
    std::cout << "*ActionRolesReader: - content: " << this->content << std::endl;
    std::cout << "*ActionRolesReader: - stuff: " << this->stuff << std::endl;
    std::cout << "*ActionRolesReader: - goal: " << this->goal << std::endl;
    std::cout << "*ActionRolesReader: - amount: " << this->amount << std::endl;
    std::cout << "*ActionRolesReader: - unit: " << this->unit << std::endl;
    std::cout << "*ActionRolesReader: - action_verb: " << this->actionVerb << std::endl;
}

//////////////////////////////////////////////////
void ActionRolesReader::Init()
{
    // Initialize the transport node
    this->gznode = transport::NodePtr(new transport::Node());    
    // TODO check how would it be possible to get the world name
    this->gznode->Init("chemlab_world"); 
        
    // Publish custom event msgs
    this->customEventPub = this->gznode->Advertise<msgs::GzString>("~/custom_events");
    
    // Publish prac event msgs
    this->pracEventPub = this->gznode->Advertise<msgs::GzString>("~/prac_events");
    
    // Initialize variables after connected to the world
    this->worldCreatedConnection =  event::Events::ConnectWorldUpdateBegin<>(
            boost::bind(&ActionRolesReader::InitOnWorldConnect, this));
}

//////////////////////////////////////////////////
void ActionRolesReader::InitOnWorldConnect()
{
    // wait for connection
    this->pracEventPub->WaitForConnection();    
    
    // lambda function to publish the adts
    auto pub_adt = [this] (std::string _type, std::string _msg) 
    {
        if(!_msg.empty())
        {
            msgs::GzString gzstr_msg;                
            gzstr_msg.set_data(std::string("adt,") + _type + std::string(",") + _msg);
            // publish the msg
            this->customEventPub->Publish(gzstr_msg);            
            // publish the prac event
            this->pracEventPub->Publish(gzstr_msg);
        }
    };

    // publish adt custom msgs
    pub_adt("instruction",this->instruction);
    pub_adt("action_core",this->actionCore);
    pub_adt("content",this->content);
    pub_adt("stuff",this->stuff);
    pub_adt("goal",this->goal);
    pub_adt("unit",this->unit);
    pub_adt("amount",this->amount);
    pub_adt("action_verb",this->actionVerb);
    
    // destroy connection
    this->worldCreatedConnection->~Connection();
}






















