/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2016, Andrei Haidu,
 *  Institute for Artificial Intelligence, Universität Bremen.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Institute for Artificial Intelligence,
 *     Universität Bremen, nor the names of its contributors may be
 *     used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "CustomEventWriter.hh"
#include "gazebo/transport/transport.hh"
#include "gazebo/util/LogRecord.hh"

using namespace sim_games;
using namespace gazebo;

// Register this plugin with the simulator
GZ_REGISTER_WORLD_PLUGIN(CustomEventWriter)

//////////////////////////////////////////////////
CustomEventWriter::CustomEventWriter()
{
}

//////////////////////////////////////////////////
CustomEventWriter::~CustomEventWriter()
{
}

//////////////////////////////////////////////////
void CustomEventWriter::Load(gazebo::physics::WorldPtr _parent, sdf::ElementPtr _sdf)
{
    // Initialize the transport node
    this->gznode = transport::NodePtr(new transport::Node());
    this->gznode->Init(_parent->GetName());
    
    // Subscribe to hydra topic
    this->customEventSub = this->gznode->Subscribe("~/custom_events",
            &CustomEventWriter::OnCustomEventMsg, this);    
    
    std::cout << "******** CUSTOM EVENT WRITER LOADED *********" << std::endl;
}

//////////////////////////////////////////////////
void CustomEventWriter::OnCustomEventMsg(ConstGzStringPtr& _msg)
{
    if(_msg->data() == "start_logging")
    {        
        // store the log file path
         this->logFilePath = util::LogRecord::Instance()->Filename();
    }
    else if(_msg->data() == "stop_logging" && this->customEvents.size() > 0)
    {
        // append the events to the end of the file        
        // create file
        std::ofstream log_file;
        
        // open and add values to the file
        log_file.open(this->logFilePath, std::ios_base::app);
        
        // start custom events xml tag
        log_file << "\n<custom_events>\n";
        
        // add saved custom events
        for(auto event : this->customEvents)
        {
            log_file << "\t<event>" << event << "</event>" << std::endl;
        }
        
        // close xml tag
        log_file << "</custom_events>\n";     
        
        // close file
        log_file.close();
    }
    else
    {
        this->customEvents.push_back(_msg->data());
    }
}